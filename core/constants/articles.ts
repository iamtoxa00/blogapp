export const articles = [
  {
    date: '12.02.22',
    title: 'Что такое облачные технологии и почему их используют девять компаний из десяти',
    content: require('./articlesdata/1.md').default
  },
  {
    coverImg: '/images/articles/2-cover.jpg',
    date: '17.02.22',
    title: 'Обзор облачных систем от российских компаний',
    content: require('./articlesdata/2.md').default
  },
  {
    coverImg: '/images/articles/3-cover.jpeg',
    date: '28.02.22',
    title: 'Масштабирование облака',
    content: require('./articlesdata/3.md').default
  },
  {
    coverImg: '/images/articles/4-cover.gif',
    date: '24.03.22',
    title: 'Тенденция развития облачной инфраструктуры',
    content: require('./articlesdata/4.md').default
  }
]