export const bookmarks = [
  {
    label: 'Создание собственного React с нуля',
    url: 'https://habr.com/ru/company/otus/blog/653861/'
  },
  {
    label: 'NFT, NFT, NFT',
    url: 'https://habr.com/ru/company/ruvds/blog/653665/'
  },
  {
    label: 'TypeScript тип any против всех',
    url: 'https://habr.com/ru/post/653779/'
  },
  {
    label: 'Как выбрать подходящий обучающий курс?',
    url: 'https://habr.com/ru/post/653731/' 
  }
]