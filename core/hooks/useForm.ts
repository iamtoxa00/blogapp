import { TFunc } from "core/types/types";
import React, { FormEventHandler, useCallback, useEffect, useState } from "react";

export function useForm(onSubmit: TFunc, initialValues:any | undefined = {}) {
  const [formData, setFormData] = useState<any>({});
  const onChangeField = useCallback((fieldName)=>(newValue: string | number)=>{
    setFormData((prevState:any) => {
      return {
        ...prevState,
        [fieldName]: newValue
      };
    });
  }, []);

  const handleSubmit: ()=>void = useCallback((e:any)=>{
    if(e) e.preventDefault();
    if(onSubmit) onSubmit(formData);
  }, [onSubmit, formData]) as any

  useEffect(()=>{
    if(initialValues){
      Object.keys(initialValues).forEach((key)=>{
        if(formData[key] === undefined && initialValues[key]) {
          onChangeField(key)(initialValues[key]);
        }
      })
    }
  }, [initialValues, formData, onChangeField])

  return {
    formData,
    onChangeField,
    handleSubmit
  }
}