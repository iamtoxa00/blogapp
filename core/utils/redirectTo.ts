import Router from 'next/router'

const redirectTo = (url: string, external = false): void => {
    if(external) {
        window.open(url);
        return;
    }
    if (!url) { window.scrollTo(0, 0); }
    Router.push(url)
    window.scrollTo(0, 0);
};

export default redirectTo;
