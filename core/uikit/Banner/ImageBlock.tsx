import Image from '../Image';
import styles from './ImageBlock.module.scss';

interface IProps {
    href: string;
    image: string;
    alt?: string;
}

const Banner:React.FC<IProps> = ({
    href,
    image,
    alt
}) => (
    <a href={href} target="_blank" rel="noreferrer" className={styles.block}>
        <Image src={image} alt={alt || ""} />
    </a>
);

export default Banner;