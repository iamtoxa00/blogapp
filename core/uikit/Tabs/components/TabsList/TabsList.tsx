import { useCallback, useEffect, useMemo, useState } from "react";
import classNames from 'classnames';
import styles from './TabsList.module.scss';
import { useRouter } from "next/router";
import { TFunc } from "core/types/types";

interface ITab {
  label: string;
  content: React.ReactNode | String | TFunc;
}

interface IProps {
  tabs: ITab[];
  id: string;
}

const TabsList: React.FC<IProps> = ({
  tabs,
  id
}) => {
  const [currTabIndex, setIndex] = useState<number | undefined>(undefined);
  const router = useRouter();

  const handleOpenTab = useCallback((index) => ()=> {
    if(typeof tabs[index].content === 'string') {
      router.replace(tabs[index].content as string)
    } else if (typeof tabs[index].content === 'function') {
      (tabs[index].content as Function)();
    } else {
      setIndex(index);
      router.replace(`${router.basePath}#${id}-${index}`)
    }
  }, [id, router, tabs])

  useEffect(()=>{
    const hash = router.asPath.split('#')[1];

    if(hash) {
      const [hashid, hashindex] = hash.split('-');
      if(hashid == id && hashindex !== undefined) {
        setIndex(Number(hashindex))
      }
    } else {
      setIndex(0)
    }
  }, [])

  const currTab = useMemo(() => currTabIndex !== undefined && tabs[currTabIndex], [tabs, currTabIndex])

  return (
    <div className={styles.tabs}>
      <div className={styles.tabs__list}>
        {tabs.map((tab, index)=>(
          <button 
            key={index}
            className={classNames(styles.tabs__tab, {
              [styles['tabs__tab--active']]: index === currTabIndex
            })}
            onClick={handleOpenTab(index)}
          >
            {tab.label}
          </button>
        ))}
      </div>
      {currTab != undefined && currTab !== false && currTab.content && (
        <div className={styles.tabs__inner}>
          {currTab.content}
        </div>
      )}
    </div>
  )
}

export default TabsList;