import { useForm } from 'core/hooks/useForm';
import { toBase64 } from 'core/utils/blobToBase64';
import redirectTo from 'core/utils/redirectTo';
import Cookies from 'js-cookie';
import { useCallback, useEffect, useState } from 'react';
import Button from '../Button';
import Input from '../Input';
import styles from './ProfileEditor.module.scss';
import CoverInput from './components/CoverInput';

interface IProps {
  userData: IUser;
}

const ProfileEditor: React.FC<IProps> = ({
  userData
}) => {
  const [cover, setCover] = useState<string>();
  const onCoverChange = async (newFile: File | undefined) => {
    if (!newFile) {
      setCover(undefined)
      return
    }

    const objectUrl = await toBase64(newFile) as any
    setCover(objectUrl)
  }

  const onSubmit = async (formData: any) => {
    const loginRes = await fetch(`https://blogapi.m-dev.studio/user`, {
      method: "PATCH",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Cookies.get('token')}`
      },
      body: JSON.stringify({
        name: formData['name'],
        birth_datetime: formData['birth_datetime'],
        image: formData['image'],
        cover: cover,
        description: formData['description'],
        work: formData['work'],
        about: formData['about']
      })
    })
    
    redirectTo('/profile');
  }

  const {
    onChangeField,
    formData,
    handleSubmit
  } = useForm(onSubmit)


  useEffect(()=>{
    onChangeField('name')(userData.name)
    onChangeField('birth_datetime')(userData.birth_datetime!)
    onChangeField('image')(userData.image!)
    onChangeField('description')(userData.description!)
    onChangeField('work')(userData.work!)
    onChangeField('about')(userData.about!)
    setCover(userData.cover);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <div className={styles.panel}>
        <CoverInput
          onChange={onCoverChange}
          value={cover}
          label="Выберите или перетащите изображение для обложки профиля"
        />
        <Input
          label='Имя'
          name='name'
          onChange={onChangeField('name')}
          value={formData['name']}
        />
        <Input
          label='Дата рождения'
          type="date"
          name='birth_datetime'
          onChange={onChangeField('birth_datetime')}
          value={formData['birth_datetime']}
        />
        <Input
          label='Краткое описание'
          name='description'
          onChange={onChangeField('description')}
          value={formData['description']}
        />
        <Input
          label='Место работы'
          name='work'
          onChange={onChangeField('work')}
          value={formData['work']}
        />
        <Input
          label='О себе'
          name='about'
          onChange={onChangeField('about')}
          value={formData['about']}
          area
        />

        <Button onClick={handleSubmit} size="md" theme='primary'>
          Сохранить
        </Button>
      </div>
    </>
  )
}

export default ProfileEditor;