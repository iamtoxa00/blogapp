import Image from 'core/uikit/Image';
import { ChangeEventHandler, useEffect, useState } from 'react';
import styles from './CoverInput.module.scss';

interface IProps {
  label?: string;
  value?: string;
  onChange: (newValue: File | undefined) => void;
}

const CoverInput: React.FC<IProps> = ({
  label = 'Загрузите изображение',
  onChange,
  value
}) => {
  const handleInput: ChangeEventHandler<HTMLInputElement> = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      onChange(undefined)
      return
    }

    // I've kept this example simple by using the first image instead of multiple
    onChange(e.target.files[0])
  }

  return (
    <>
      <label className={styles.input}>
        {!value && <span>{label}</span>}
        <input type="file" name="" id="" onChange={handleInput} />
        {value && <Image src={value} alt="" />}
      </label>
    </>
  )
}

export default CoverInput;