import { useForm } from 'core/hooks/useForm';
import { useCallback, useState } from 'react';
import Input from '../Input';
import styles from './ArticleEditor.module.scss';

import MdEditor from 'react-markdown-editor-lite';
import 'react-markdown-editor-lite/lib/index.css';
import MarkDownBody from '../Article/components/ArticleBody/MarkDownBody/MarkDownBody';
import CoverInput from '../ProfileEditor/components/CoverInput';
import { toBase64 } from 'core/utils/blobToBase64';
import Button from '../Button';
import redirectTo from 'core/utils/redirectTo';
import Cookies from 'js-cookie';

const ArticleEditor: React.FC = () => {
  const [text, setText] = useState();
  const [cover, setCover] = useState<string>();
  const onCoverChange = async (newFile: File | undefined) => {
    if (!newFile) {
      setCover(undefined)
      return
    }

    const objectUrl = await toBase64(newFile) as any
    setCover(objectUrl)
  }

  const onSubmit = async (formData: any) => {
    const addArticleRes = await fetch(`https://blogapi.m-dev.studio/article`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Cookies.get('token')}`
      },
      body: JSON.stringify({
        title: formData['title'],
        content: text,
        cover: cover,
      })
    })
    
    redirectTo('/profile');
  }

  const {
    onChangeField,
    formData,
    handleSubmit
  } = useForm(onSubmit)

  const handleEditorChange = useCallback(({ html, text }) => {
    setText(text);
  }, [])

  return (
    <>
      <div className={styles.panel}>
        <CoverInput
          onChange={onCoverChange}
          value={cover}
          label="Выберите или перетащите изображение для обложки"
        />
        <Input
          label='Название статьи'
          name='title'
          onChange={onChangeField('title')}
          value={formData['title']}
        />
      </div>
      <div className={styles.panel}>
        <MdEditor
          style={{ height: '500px' }}
          renderHTML={text => (
            <MarkDownBody
              content={text}
            />
          )}
          onChange={handleEditorChange}
        />

        <Button onClick={handleSubmit} size="md" theme='primary'>
          Сохранить
        </Button>
      </div>
    </>
  )
}

export default ArticleEditor;