import { useCallback, useEffect, useState } from "react";
import Button from "../Button";
import ModalContainer from "../ModalContainer";
import UserModal from "./components/UserModal";
import styles from './UserBtn.module.scss';
import redirectTo from "core/utils/redirectTo";
import Cookies from 'js-cookie';

const UserBtn: React.FC = ()=>{
  const [token, setToken] = useState<string | null>();
  const [showModal, setShowModal] = useState(false);

  useEffect(()=>{
    const token = Cookies.get('token');
    setToken(token);
    console.log(token)
  }, []);

  const handleLogin = useCallback(()=>{
    setShowModal(true);
  }, [])
  const handleCloseLogin = useCallback(()=>{
    setShowModal(false);
  }, [])

  const handleGoToPorofile = ()=>{
    redirectTo(`/profile`)
  }

  return (
    <>
      {showModal && <ModalContainer
        onClose={handleCloseLogin}
      >
        <UserModal onClose={handleCloseLogin}/>
      </ModalContainer>}
      <div className={styles.wrapper}>
        {!!token ? (
          <Button onClick={handleGoToPorofile}>
            go to profile
          </Button>
        ) : (
          <Button onClick={handleLogin}>
            login
          </Button>
        )}
      </div>
    </>
  )
}

export default UserBtn;