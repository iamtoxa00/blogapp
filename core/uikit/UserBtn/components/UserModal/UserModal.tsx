import { useForm } from "core/hooks/useForm";
import { TFunc } from "core/types/types";
import Button from "core/uikit/Button";
import Input from "core/uikit/Input";
import { ModalForm } from "core/uikit/ModalContainer";
import { useCallback, useEffect, useState } from "react";
import styles from './UserModal.module.scss';
import Cookies from 'js-cookie';

interface IProps {
  onClose: TFunc;
}

const UserBtn: React.FC<IProps> = ({
  onClose
}) => {
  const [isLogin, setIsLogin] = useState(true);

  const onSubmit = async () => {
    if (isLogin) {
      const loginRes = await fetch(`https://blogapi.m-dev.studio/auth/login`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: formData['username'],
          password: formData['pass']
        })
      })
      const loginData = await loginRes.json()

      if (loginRes && loginData?.access_token) {
        Cookies.set('token', loginData['access_token']);
        window.location.reload();
      }
    } else {
      
    }
  }

  const onSwitch = () => {
    setIsLogin(!isLogin);
  }

  const {
    formData,
    handleSubmit,
    onChangeField
  } = useForm(onSubmit);

  return (
    <ModalForm>
      {!isLogin && (
        <Input
          label="Имя"
          name="name"
          value={formData['name']}
          onChange={onChangeField('name')}
        />
      )}
      <Input
        label="Почта"
        name="email"
        value={formData['username']}
        onChange={onChangeField('username')}
      />
      <Input
        label="Пароль"
        name="password"
        type="password"
        value={formData['pass']}
        onChange={onChangeField('pass')}
      />
      <Button size="md" theme='primary' onClick={handleSubmit}>
        {isLogin ? "Войти" : "Создать аккаунт"}
      </Button>
      <Button onClick={onSwitch}>
        {isLogin ? "или Зарегестрируйтесь" : "или Войдите"}
      </Button>
    </ModalForm>
  )
}

export default UserBtn;