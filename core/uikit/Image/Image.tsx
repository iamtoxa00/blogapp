import classNames from "classnames";
import { TFunc } from "core/types/types";
import { useCallback, useState } from "react";

import styles from './Image.module.scss';

/* eslint-disable @next/next/no-img-element */
interface IProps {
  src: string;
  alt?: string;
  className?: string;
  onClick?: TFunc;
  lightbox?: boolean;
  title?: string;
}

const Image: React.FC<IProps> = ({
  src,
  alt = "",
  className,
  onClick,
  lightbox = false,
  title
})=>{
  const [error, setError] = useState(false);

  const [showLightbox, setLightbox] = useState(false);

  const handleClick = useCallback(()=>{
    if (lightbox) {
      setLightbox(true);
    } else {
      if (onClick) onClick();
    }
  }, [lightbox, onClick]);

  const handleClickLightbox = useCallback(()=>{
      setLightbox(false);
  }, []);

  return (
    <>
        {showLightbox && (
          <div onClick={handleClickLightbox} className={styles.lightbox}>
            <img title={title} src={src} alt={alt} className={styles.lightboxImg}/>
          </div>
        )}
        <img title={title} onClick={handleClick} src={src} alt={alt} className={classNames(className, {
          [styles['image--error']]: error
        })} onError={()=>{setError(true)}}/>
    </>
  )
}

export default Image;