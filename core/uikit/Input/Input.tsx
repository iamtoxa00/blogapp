import styles from "./Input.module.scss";
import cn from 'classnames';
import { ChangeEvent, FC, useEffect, useMemo, useRef, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

type TTypes = 'password' | 'text' | 'date';

interface IProps {
  label: string;
  type?: TTypes,
  name: string,
  onChange?: (newValue: string, ev: ChangeEvent)=>void
  value?: string;
  area?: boolean;
}

const Input:FC<IProps> = ({
  type = 'text',
  label,
  name,
  onChange,
  value: controlledValue,
  area
}) => {
  const [focus, setFocus] = useState(false);
  const [value, setValue] = useState('');

  const fieldValue = useMemo(()=>{
    return controlledValue || value;
  }, [value, controlledValue])

  const handleChange = (e: ChangeEvent)=>{
    const value = (e.target as HTMLInputElement).value;
    setValue(value);
    if(onChange) onChange(value, e)
  }

  const handleDateChange = (date:Date) => {
    if(onChange) onChange(Number(date), null)
  }

  if(type === 'date') {
    return (
      <div className={cn(styles.input, {
        [styles.input_focused]: true,
        [styles.input_filled]: fieldValue
      })}>
        <span className={styles.label}>{label}</span>
          <DatePicker
            className={styles.field}
            selected={fieldValue ? new Date(fieldValue) : undefined}
            name={name}
            showYearDropdown
            onChange={handleDateChange}
          />
      </div>
    );
  }

  return (
    <div className={cn(styles.input, {
      [styles.input_focused]: focus,
      [styles.input_filled]: fieldValue
    })}>
      <span className={styles.label}>{label}</span>
      {!area ? (
        <input
          className={styles.field}
          type={type}
          value={fieldValue}
          name={name}
          autoComplete="new-password"
          onFocus={()=>{setFocus(true)}}
          onBlur={()=>{setFocus(false)}}
          onChange={handleChange}
        />
      ) : (
        <textarea
          className={styles.field}
          name={name}
          autoComplete="new-password"
          onFocus={()=>{setFocus(true)}}
          onBlur={()=>{setFocus(false)}}
          onChange={handleChange}
          value={fieldValue}
        />
      )}
    </div>
  );
};

export default Input;
