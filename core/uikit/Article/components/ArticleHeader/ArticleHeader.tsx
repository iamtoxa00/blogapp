import Image from 'core/uikit/Image';
import redirectTo from 'core/utils/redirectTo';
import { useCallback } from 'react';
import styles from './ArticleHeader.module.scss';

interface IProps {
  title: string;
  authorName: string;
  authorImg?: string;
  coverImg?: string;
  date: string;
}

const ArticleHeader: React.FC<IProps> = ({
  title,
  authorName,
  authorImg,
  date,
  coverImg,
}) => {
  const handleOpenAccount = useCallback(()=>{
    redirectTo('/profile#profileTab-1');
  }, [])

  return (
    <div className={styles.articleHeader}>
      {coverImg && (
        <Image
          className={styles.articleHeader__cover}
          src={coverImg}
          alt=""
        />
      )}
      <h1 className={styles.articleHeader__title}>{title}</h1>
      <hr className={styles.articleHeader__hr} />
      <div className={styles.articleHeader__authorWrapper}>
        {authorImg && (
          <Image
            src={authorImg}
            alt=""
            className={styles.articleHeader__authorImg}
            onClick={handleOpenAccount}
          />
        )}
        <span onClick={handleOpenAccount} className={styles.articleHeader__authorName}>{authorName}</span>
        <span className={styles.articleHeader__authorDate}>{date}</span>
      </div>
    </div>
  )
}

export default ArticleHeader;