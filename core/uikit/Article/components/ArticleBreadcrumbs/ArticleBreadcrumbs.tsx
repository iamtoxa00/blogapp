import { useMemo } from 'react';
import Link from 'next/link';
import styles from './ArticleBreadcrumbs.module.scss';

interface IProps {
  title: string;
}

const ArticleBreadcrumbs: React.FC<IProps> = ({
  title
}) => {
  const items = useMemo(() => [
    {
      label: 'Статьи',
      href: '/profile#profileTab-1'
    },
    {
      label: title,
      href: '#'
    }
  ], [title]);

  return (
    <div className={styles.articleBreadcrumbs}>
      {items.map((item, index) => (
        <Link
          key={index}
          href={item.href}
        >
          {item.label}
        </Link>
      ))}
    </div>
  )
}

export default ArticleBreadcrumbs;