/* eslint-disable react/no-children-prop */
import styles from '../ArticleBody.module.scss';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import { useCallback, useMemo } from 'react';
import raw from "rehype-raw";
import Image from 'core/uikit/Image';

import { Line, Bar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  BarElement,
  Title,
  Tooltip,
  Legend

} from 'chart.js';
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  BarElement,
  Title,
  Tooltip,
  Legend
);

interface IProps {
  content: string;
}


function ImageRenderer(props: any) {
  const imageSrc = props.src;
  const altText = props.title;
  return (
    <Image
      src={imageSrc}
      alt={altText || ""}
      title={altText}
      lightbox
    />
  );
}

const MarkDownBody: React.FC<IProps> = ({
  content
}) => {
  const getCanvasRenderer = useCallback((props) => {
    const dataset = JSON.parse(props.children);
    const id = (Math.random() * 100000).toString()
    return (
      <>
        {dataset.type === 'line' ? (
          <Line
            datasetIdKey={id}
            data={dataset.data}
            options={dataset.options}
          />
        ) : (
          <Bar
            datasetIdKey={id}
            data={dataset.data}
            options={dataset.options}
          />
        )}
      </>
    );
  }, []);

  return (
    <ReactMarkdown
      className={styles.article}
      rehypePlugins={[raw]}
      remarkPlugins={[remarkGfm]}
      remarkRehypeOptions={{ allowDangerousHtml: true }}
      children={content}
      components={{
        'img': ImageRenderer,
        'canvas': getCanvasRenderer
      }}
    />
  )
}

export default MarkDownBody;