/* eslint-disable react/no-children-prop */
import styles from './ArticleBody.module.scss';
import { useCallback, useEffect, useState } from 'react';
import MarkDownBody from './MarkDownBody/MarkDownBody';

interface IProps {
  content: string;
}

const ArticleBody: React.FC<IProps> = ({
  content
}) => {
  const [showUp, setShowUp] = useState(false);

  const handleScrollUp = useCallback(()=>{
    window.scroll(0, 0)
  }, [])

  const handleScroll = useCallback(()=>{
    if(window.scrollY > 200) {
      setShowUp(true);
    } else {
      setShowUp(false)
    }
  }, [])

  useEffect(()=>{
    window.addEventListener('scroll', handleScroll)

    return ()=>{
      window.removeEventListener('scroll', handleScroll)
    }
  })

  useEffect(()=>{
    var d = document, s = d.createElement('script');
    s.src = 'https://mdev-blog.disqus.com/embed.js';
    s.setAttribute('data-timestamp', (+new Date()).toString());
    (d.head || d.body).appendChild(s);
  },[])

  return (
    <div className={styles.articleBody}>
      <MarkDownBody
          content={content}
      />
      {showUp && <button className={styles.showUpBtn} onClick={handleScrollUp}>UP</button>}
      <div id="disqus_thread"></div>
    </div>
  )
}

export default ArticleBody;