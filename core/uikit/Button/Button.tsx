import React, { ButtonHTMLAttributes, CSSProperties } from 'react';
import cn from 'classnames';

import styles from './Button.module.scss';
import { TFunc } from 'core/types/types';

type size = 'sm' | 'md' | 'lg';
type theme = 'primary' | 'secondary';
type variant = 'outline' | 'filled';

interface IButtonProps {
  size?: size;
  className?: string;
  theme?: theme;
  round?: boolean;
  style?: CSSProperties;
  variant?: variant;
  type?: 'submit' | 'reset' | 'button' | undefined;
  onClick?: TFunc;
}

const Button: React.FC<IButtonProps> = ({
  children,
  size,
  className,
  theme,
  round = false,
  variant = 'filled',
  style,
  type = 'button',
  onClick
}) => {
  return <button
    type={type}
    className={cn(
      [
        styles.Button,
        className
      ],
      {
        [styles[`Button--size-${size}`]]: !!size,
        [styles[`Button--theme-${theme}`]]: !!theme,
        [styles[`Button--variant-${variant}`]]: !!variant,
        [styles['Button--round']]: round
      }
    )}
    style={style}
    onClick={onClick}
  >
    <div className={styles.Button__container}>
      {children}
    </div>
  </button>
}

export default Button;