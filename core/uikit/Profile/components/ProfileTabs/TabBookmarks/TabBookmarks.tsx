import { bookmarks } from "core/constants/bookmarks";
import ArticleCard from "../TabArticles/components/ArticleCard/ArticleCard";

import styles from './TabBookmarks.module.scss';

const TabBookmarks: React.FC = ()=>{
  return (
    <div className={styles.tabBookmarks}>
      {bookmarks.map((item, index)=>(
        <ArticleCard
          key={index}
          title={item.label}
          url={item.url}
          external={true}
        />
      ))}
    </div>
  )
}

export default TabBookmarks;