import moment from 'moment';
import { articles } from "core/constants/articles";
import ArticleCard from "./components/ArticleCard/ArticleCard";

import styles from './TabArticles.module.scss';

interface IProps {
  articlesData: IArticle[];
}

const TabArticles: React.FC<IProps> = ({
  articlesData
})=>{
  return (
    <div className={styles.tabArticles}>
      {articlesData?.map((item, index)=>(
        <ArticleCard
          key={index}
          title={item.title}
          date={moment(item.pub_datetime).format('DD.MM.yyyy')}
          cover={item.cover}
          url={`/article/${item._id}`}
        />
      ))}
    </div>
  )
}

export default TabArticles;