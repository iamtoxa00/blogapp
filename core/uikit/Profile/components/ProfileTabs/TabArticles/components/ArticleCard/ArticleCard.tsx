import Image from "core/uikit/Image";
import redirectTo from "core/utils/redirectTo";
import { useCallback, useMemo } from "react";

import styles from './ArticleCard.module.scss';

interface IProps {
  description?: string;
  title: string;
  cover?: string;
  date?: string;
  url: string;
  external?: boolean;
}

const ArticleCard: React.FC<IProps> = ({
  title,
  description,
  cover,
  date,
  url,
  external = false
})=>{
  const handleOpen = useCallback(()=>{
    redirectTo(url, external);
  }, [url, external])

  return (
    <div className={styles.articleCard} onClick={handleOpen}>
      <Image src={cover || ""} alt="" className={styles.articleCard__cover}/>
      <span className={styles.articleCard__title}>{title}</span>
      {description && (
        <p>
          {description}
        </p>
      )}
      <hr />
      <div className={styles.articleCard__footer}>
        {date && <span className={styles.articleCard__date}>{date}</span>}
        <span className={styles.articleCard__readBtn}>Читать дальше...</span>
      </div>
    </div>
  )
}

export default ArticleCard;