import styles from './ProfileBadge.module.scss'

interface IProps {
  label: string;
  value: string;
}

const ProfileBadge: React.FC<IProps> = ({
  label,
  value
}) => {
  return (
    <div className={styles.profileBadge}>
      <span className={styles.profileBadge__label}>{label}</span>
      <span className={styles.profileBadge__value}>{value}</span>
    </div>
  )
}

export default ProfileBadge;