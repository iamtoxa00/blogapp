import { links } from "./constant";

import styles from './ProfileLinks.module.scss'

const ProfileHeader: React.FC = () => {
  return (
    <div className={styles.profileLinks}>
      <span className={styles.profileLinks__title}>Личные страницы</span>
      <div className={styles.profileLinks__list}>
        {links.map((item, index)=>(
          <a key={index} href={item.url} className={styles.profileLinks__item}>{item.label}</a>
        ))}
      </div>
    </div>
  )
}

export default ProfileHeader;