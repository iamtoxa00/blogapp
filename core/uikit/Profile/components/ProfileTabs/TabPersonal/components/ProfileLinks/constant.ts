export const links = [
  {
    label: 'Instagram',
    url: 'https://www.instagram.com/itech.bear/'
  },
  {
    label: 'ВКонтакте',
    url: 'https://vk.com/iam_toxa'
  },
  {
    label: 'HeadHunter',
    url: 'https://hh.ru/resume/965a948cff05ed33c90039ed1f624c35325970'
  },
  {
    label: 'Email',
    url: 'mailto://imatoxa00@gmail.com'
  }
]