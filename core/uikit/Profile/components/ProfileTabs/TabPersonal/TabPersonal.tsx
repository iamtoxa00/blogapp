import moment from 'moment';

import { bookmarks } from 'core/constants/bookmarks';
import ProfileBadge from './components/ProfileBadge/ProfileBadge';
import ProfileLinks from './components/ProfileLinks/ProfileLinks'

import styles from './TabPersonal.module.scss'

interface IProps {
  userdata: IUser;
  articlesData: IArticle[];
}

const TabPersonal: React.FC<IProps> = ({
  userdata,
  articlesData
})=>{
  moment.locale('ru');

  return (
    <div className={styles.TabPersonal}>
      <div className={styles.badges}>
        <ProfileBadge label="Откуда" value="Россия"/>
        <ProfileBadge label="Работает в" value={userdata.work || '-'}/>
        <ProfileBadge label="Дата рождения" value={userdata.birth_datetime ? moment(userdata.birth_datetime).format('DD.MM.yyyy') : '-'}/>
        <ProfileBadge label="Зарегестрирован" value={moment(userdata.reg_datetime).format('DD.MM.yyyy')}/>
        <ProfileBadge label="Статьи" value={articlesData.length.toString()}/>
        <ProfileBadge label="Закладки" value={bookmarks.length.toString()}/>
      </div>

      {userdata.about && <div className={styles.about}>
        <span className={styles.about__title}>О себе</span>
        <div className={styles.about__content}>
          {userdata.about}
        </div>
      </div>}

      <ProfileLinks/>
    </div>
  )
}

export default TabPersonal;