import { useEffect } from "react";

const TabComments: React.FC = ()=>{
  
  useEffect(()=>{
    var d = document, s = d.createElement('script');
    s.src = 'https://mdev-blog.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
  },[])

  return (
    <>
      <div id="disqus_thread"></div>
    </>
  )
}

export default TabComments;