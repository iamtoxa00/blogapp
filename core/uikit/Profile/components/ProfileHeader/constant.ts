export const ProfileInfo = {
  name: "Антон Медведев",
  description: 'Ведущий разработчик РСХБ и студент группы 181-723',
  image: "/images/author.jpg",
  cover: "/images/author_cover.jpg"
}