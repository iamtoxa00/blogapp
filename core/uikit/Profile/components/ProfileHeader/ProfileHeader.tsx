import Image from "core/uikit/Image";
import Banner from "core/uikit/Banner";

import styles from './ProfileHeader.module.scss'

interface IProps {
  userdata: IUser;
}

const ProfileHeader: React.FC<IProps> = ({
  userdata
}) => {
  return (
    <>
      <div className={styles.profileHeader}>
        {userdata.cover && <Image 
          src={userdata.cover}
          alt=""
          className={styles.profileHeader__cover}
          lightbox
        />}
        <div className={styles.profileHeader__authorWrapper}>
          {userdata.image && <Image
            src={userdata.image}
            alt=""
            className={styles.profileHeader__authorImg}
          />}
          <span className={styles.profileHeader__authorName}>
            {userdata?.name}
          </span>
        </div>
        {userdata.description && (
          <>
            <hr className={styles.profileHeader__hr} />
            <span className={styles.profileHeader__description}>
              {userdata.description}
            </span>
          </>
        )}
      </div>
      <Banner
        href="https://cloud.yandex.ru/"
        alt="Надёжное облако для вашего бизнеса"
        image="/images/ImageBlock.gif"
      />
    </>
  )
}

export default ProfileHeader;