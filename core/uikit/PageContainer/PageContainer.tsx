import styles from "./PageContainer.module.scss";

const PageContainer: React.FC = ({
  children
}) => { 
  return (
    <main className={styles.pagecontainer}>
      {children}
    </main>
  )
}

export default PageContainer;