interface IArticle {
  _id: string;
  title: string;
  content: string;
  pub_datetime: number;
  cover?: string;
  author: string;
}