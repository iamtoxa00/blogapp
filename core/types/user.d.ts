interface IUser {
  _id: string;
  email: string;
  name: string;
  reg_datetime: number;
  birth_datetime?: number;
  image?: string;
  cover?: string;
  description?: string;
  work?: string;
  about?: string;
}