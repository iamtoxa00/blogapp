module.exports = {
  typescript: {
    ignoreBuildErrors: true,
  },
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.module.rules = [
      ...config.module.rules,
      {
        test: /\.md$/,
        use: 'raw-loader'
      }
    ]
    return config
  },
}