import type { NextPage } from "next";

const Home: NextPage = () => {
  return (
    <div>
      Main Page | Redirect to profile
    </div>
  );
};


export async function getServerSideProps() {
  return {
    redirect: {
      destination: '/profile',
      permanent: false,
    },
  }
}

export default Home;
