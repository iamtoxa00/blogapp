import PageContainer from 'core/uikit/PageContainer';
import ProfileEditor from 'core/uikit/ProfileEditor';
import ArticleBreadcrumbs from 'core/uikit/Article/components/ArticleBreadcrumbs/ArticleBreadcrumbs';
import { GetServerSideProps, NextPage } from 'next';
import jwt from 'jsonwebtoken';

interface IProps {
  userData: IUser;
}

const ProfilePage: NextPage<IProps> = ({
  userData
}) => {
  return (
    <PageContainer>
      <ArticleBreadcrumbs
        title="Редактор профиля"
      />
      <ProfileEditor userData={userData}/>
    </PageContainer>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const token = context.req.cookies['token'];
  if(token) {
    const payload = jwt.decode(token) as any;

    const resuserData = await fetch(`https://blogapi.m-dev.studio/user/${payload.sub}`)
    const userData = await resuserData.json()
  
    return { props: { userData } }
  }

  context.res.setHeader('Location', `/article`) // Replace <link> with your url link
  context.res.statusCode = 302
  context.res.end()
  return { props: { } }
}

export default ProfilePage