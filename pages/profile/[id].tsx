import PageContainer from "core/uikit/PageContainer";
import { ProfileHeader, TabArticles, TabBookmarks, TabComments, TabPersonal } from "core/uikit/Profile";
import { TabsList } from "core/uikit/Tabs";
import { GetServerSideProps, NextPage } from "next";

interface IProps {
  userData: IUser;
  articlesData: IArticle[];
}

const ProfilePage: NextPage<IProps> = ({
  userData,
  articlesData
}) => {
  return (
    <PageContainer>
      <ProfileHeader userdata={userData} />

      <TabsList
        id="profileTab"
        tabs={[
          {
            label: 'Профиль',
            content: <TabPersonal userdata={userData} articlesData={articlesData}/>
          },
          {
            label: 'Статьи',
            content: <TabArticles articlesData={articlesData}/>
          },
          {
            label: 'Комментарии',
            content: <TabComments/>
          },
          {
            label: 'Выбор редакции',
            content: <TabBookmarks/>
          }
        ]}
      />
    </PageContainer>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const resuserData = await fetch(`https://blogapi.m-dev.studio/user/${context.params!['id']}`)
  const userData = await resuserData.json()

  const resarticlesData = await fetch(`https://blogapi.m-dev.studio/article/by/${context.params!['id']}`)
  const articlesData = await resarticlesData.json()

  return { props: { userData, articlesData } }
}

export default ProfilePage;