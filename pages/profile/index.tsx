import PageContainer from "core/uikit/PageContainer";
import { ProfileHeader, TabArticles, TabBookmarks, TabComments, TabPersonal } from "core/uikit/Profile";
import { TabsList } from "core/uikit/Tabs";
import { NextPage } from "next";
import { GetServerSideProps } from 'next'
import jwt from 'jsonwebtoken';
import Cookies from "js-cookie";
import redirectTo from "core/utils/redirectTo";

interface IProps {
  userData: IUser;
  articlesData: IArticle[];
}

const ProfilePage: NextPage<IProps> = ({
  userData,
  articlesData
}) => {
  const handleLogOut = () => {
    Cookies.remove('token');
    window.location.reload();
  }

  return (
    <PageContainer>
      <ProfileHeader userdata={userData} />

      <TabsList
        id="profileTab"
        tabs={[
          {
            label: 'Профиль',
            content: <TabPersonal userdata={userData} articlesData={articlesData}/>
          },
          {
            label: 'Статьи',
            content: <TabArticles articlesData={articlesData}/>
          },
          {
            label: 'Комментарии',
            content: <TabComments/>
          },
          {
            label: 'Выбор редакции',
            content: <TabBookmarks/>
          },
          {
            label: 'Редактор',
            content: '/article/editor'
          },
          {
            label: 'Обновить профиль',
            content: '/profile/editor'
          },
          {
            label: 'Выйти',
            content: handleLogOut
          }
        ]}
      />
    </PageContainer>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const token = context.req.cookies['token'];
  if(token) {
    const payload = jwt.decode(token) as any;

    const resuserData = await fetch(`https://blogapi.m-dev.studio/user/${payload.sub}`)
    const userData = await resuserData.json()
  
    const resarticlesData = await fetch(`https://blogapi.m-dev.studio/article/by/${payload.sub}`)
    const articlesData = await resarticlesData.json()
  
    return { props: { userData, articlesData } }
  }

  context.res.setHeader('Location', `/profile/6248d6033904fb6c650a6d1c`) // Replace <link> with your url link
  context.res.statusCode = 302
  context.res.end()
  return { props: { } }
}

export default ProfilePage;