import PageContainer from 'core/uikit/PageContainer';
import ArticleEditor from 'core/uikit/ArticleEditor';
import ArticleBreadcrumbs from 'core/uikit/Article/components/ArticleBreadcrumbs/ArticleBreadcrumbs';
import { useLayoutEffect } from 'react';

const ArticlePage = () => {
  return (
    <PageContainer>
      <ArticleBreadcrumbs
        title="Редактор статей"
      />
      <ArticleEditor />
    </PageContainer>
  );
}

export default ArticlePage