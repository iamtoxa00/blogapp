import { articles } from 'core/constants/articles'
import { ArticleBody, ArticleHeader } from 'core/uikit/Article';
import ArticleBreadcrumbs from 'core/uikit/Article/components/ArticleBreadcrumbs/ArticleBreadcrumbs';
import PageContainer from 'core/uikit/PageContainer';
import { GetServerSideProps, NextPage } from 'next'
import Head from 'next/head'

interface IPage {
  article: {
    title: string;
    content: string;
    cover?: string;
    date: string;
  }
}

const ArticlePage:NextPage<IPage> = ({
  article
}) => {
  return (
    <PageContainer>
      <Head>
        <title>{article.title}</title>
      </Head>
      <ArticleHeader
        title={article.title}
        date={article.date}
        coverImg={article.cover}
        authorName="Антон Медведев"
        authorImg="/images/author.jpg"
      />
      <ArticleBreadcrumbs
        title={article.title}
      />
      <ArticleBody
        content={article.content}
      />
    </PageContainer>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const resarticlesData = await fetch(`https://blogapi.m-dev.studio/article/${context.params!['index']}`)
  const article = await resarticlesData.json()
  
  return { props: { article } }
}

export default ArticlePage