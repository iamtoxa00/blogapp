import React from 'react';
import UserBtn from "core/uikit/UserBtn";
import { AppProps } from "next/dist/shared/lib/router/router";
import "../styles/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <UserBtn/>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
